import numpy as np
'''
LinearFuzzySpace is Python package/library implementation 
of basic planar imprecise geometric objects 
(fuzzy point, fuzzy line, fuzzy triangle and fuzzy circle). 
Also, basic measurement functions 
(distance between fuzzy point and fuzzy line, 
fuzzy point and fuzzy triangle, two fuzzy lines and two fuzzy triangles) 
as well as spatial operation (linear combination of two fuzzy points) and 
main spatial relations (coincidence, between and collinear)
are implemented. 
'''

class FPoint():

    def __init__(self, c, r):
        self.c = c
        self.r = r

class FLine():

    def __init__(self, a, b):
        self.a = a
        self.b = b